#include <stdio.h>

void pattern(int a); //This function to control the pattern.
void Row(int b);     //This function for print every line of the pattern.
int NumberOfRows=1;  //This variable is used to initialize the value for number of rows.
int r;               //This variable is used to get the user input to number of rows.

void pattern(int a)
{
    if(a>0)
    {
        Row(NumberOfRows);
        printf("\n");
        NumberOfRows++;
        pattern(a-1);
    }
}
void Row(int b)
{
    if(b>0)
    {
        printf("%d",b);
        Row(b-1);
    }
}
int main()
{
    printf("Input the Number of Rows : ");
    scanf("%d",&r);
    pattern(r);
    return 0;
}

